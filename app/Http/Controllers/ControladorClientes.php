<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ControladorClientes extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cliente', ['clientes'=>Cliente::all()]);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('novocliente');
    }

    public function list()
    {
        return view('listcliente', ['clientes'=>Cliente::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //O nome do campo deve ser igual a nome do 

        // $request->validate([
        //     'nome'=>'required|min:5|max:30',
        //     'idade'=>'required|min:0|max:120',
        //     'email'=>'required|email|min:10|max:120',
        // ]);

        $regras = [
                'nome'=>'required|min:3|max:30',
                'idade'=>'required',
                'email'=>'required|email',
        ];

        // $mensagens = [
        //     'nome.required'=>'O nome é de preenchimento obrigatório.',
        //     'nome.min'=>'O nome deve ter no mínimo 5 caracteres.',
        //     'nome.max'=>'O nome não pode exceder a 30 caracteres.',
        //     'idade.required'=>'A idade é de preenchimento obrigatório.',
        //     'email.required'=>'O email é de preenchimento obrigatório.',
        //     'email.email'=>'Email inválido.'
        // ];

        $mensagens = [
            'required'=>'O campo :attribute é de preenchimento obrigatório.',
            'nome.min'=>'O nome deve ter no mínimo 5 caracteres.',
            'nome.max'=>'O nome não pode exceder a 30 caracteres.',
            'email.email'=>'Email inválido.'
        ];

        $request->validate($regras, $mensagens);
        
        $cliente = new Cliente();

        $cliente->nome = $request->input("nome");
        $cliente->idade = $request->input("idade");
        $cliente->endereco = $request->input("endereco");
        $cliente->email = $request->input("email");

        $cliente->save();

        return redirect('clientes/list');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
