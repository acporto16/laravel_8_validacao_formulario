@extends('layout', ["current" => "home"])

@section('body')

<div class="jumbotron bg-light border border-secondary">
    <div class="row">
        <div class="card-deck col-md-8">
            <div class="card border border-secondary">
                <div class="card-header">
                    <h5 class="card-title">Cadastro de Clientes</h5>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <img src="{{asset('/img/clientes.jpg')}}" alt="...">
                    </div>  
                    <div class="col-md-1">
                    </div>

                    <div class="col-md-7">  
                        <div class="card-body">                    
                            <p class="card=text">
                                Aqui você cadastra todos os seus clientes.                        
                            </p>                    
                        </div>
                    </div> 
                </div>                                      

                <div class="card-footer">
                    <a href="/clientes/list" class="btn btn-primary">Cadastre seus clientes</a>
                </div>                                

            </div>           
        </div>
    </div>
</div>

@endsection