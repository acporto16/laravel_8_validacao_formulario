@extends('layout', ["current" => "clientes"])

@section('body')
<div class="card border">
        <div class="card-body">
            <h5 class="card-title">Cadastro de Clientes</h5>
    
    @if(count($clientes) > 0)
            <table class="table table-ordered table-hover">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Nome do Cliente</th>
                        <th>Idade</th>
                        <th>Endereço</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
        @foreach($clientes as $cliente)
                    <tr>
                        <td>{{$cliente->email}}</td>
                        <td>{{$cliente->nome}}</td>
                        <td>{{$cliente->idade}}</td>
                        <td>{{$cliente->endereco}}</td>
                        <td>
                            <a href="/clientes/editar/{{$cliente->id}}" class="btn btn-sm btn-primary">Editar</a>
                            <a href="/clientes/apagar/{{$cliente->id}}" class="btn btn-sm btn-danger">Apagar</a>
                        </td>
                    </tr>
        @endforeach                
                </tbody>
            </table>
    @endif        
        </div>
        <div class="card-footer">
            <a href="/clientes/novo" class="btn btn-sm btn-primary" role="button">Novo cliente</a>
        </div>
    </div>

@endsection