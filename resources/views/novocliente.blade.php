@extends('layout', ["current" => "clientes"])

@section('body')
    <main role="main">
        <div class="row">
            <div class="container col-md-8 offset-md-2">
                <div class="card border">
                    <div class="card-body">
                        <form name="formCadastroProduto" method="POST" action="/clientes">
                            @csrf
                            {{-- Campo obrigatório. Tratamento por meio da classe is-invalid em conjunto com invalid-feedback --}}
                            <div class="form-group">
                                <label for="nome">Nome do Cliente</label>                    
                                <input type="text" id="nome" name="nome"
                                    placeholder="Informe o nome do Cliente"
                                    class="form-control {{($errors->has('nome')) ? 'is-invalid' : ''}}">

                                    @if ($errors->has('nome'))
                                        <div class="invalid-feedback">
                                            {{$errors->first('nome')}}
                                        </div>                            
                                    @endif                                    
                            </div>

                            <div class="form-group">
                                <label for="idade">Idade</label>                    
                                <input type="number " id="idade" name="idade"
                                    placeholder="Informe a quantidade em estoque" 
                                    class="form-control {{($errors->has('idade')) ? 'is-invalid' : ''}}">

                                    @if ($errors->has('idade'))
                                        <div class="invalid-feedback">
                                            {{$errors->first('idade')}}
                                        </div> 
                                    @endif                                    
                            </div>
                            
                            <div class="form-group">
                                <label for="endereco">Endereço</label>                    
                                <input type="text" class="form-control" id="endereco" name="endereco"
                                    placeholder="Informe o preço" >
                            </div>
                            
                            <div class="form-group">
                                <label for="email">Email</label>                    
                                <input type="text" id="email" name="email"
                                    placeholder="Informe o email" 
                                    class="form-control {{($errors->has('email')) ? 'is-invalid' : ''}}">

                                    @if ($errors->has('idade'))
                                        <div class="invalid-feedback">
                                            {{$errors->first('idade')}}
                                        </div> 
                                    @endif                                       
                            </div>

                            <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                            <button type="button" class="btn btn-secondary btn-sm" formnovalidate>Cancelar</button>
                        </form>
                    </div>
{{-- {{var_dump($errors)}} --}}

{{-- 
    @if ($errors->any())
        <div class="card-footer">
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{$error}}
            </div>
        @endforeach
    @endif 
                </div>
                --}}
            </div>
        </div>
    </main>
@endsection